////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file uniform_density_sphere.hpp
//! \brief A file to define useful hydro helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"
#include "thermo/base/thermo_evolve.hpp"
#include "hydro/single_zone/base/hydro.hpp"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define S_ACCELERATION_FACTOR "accel_factor"
#define S_HYDRO_EQUIL         "hydro_equil"
#define S_INITIAL_VELOCITY    "initial_velocity"
#define S_LUMINOSITY_FACTOR   "lum_factor"
#define S_MASS                "mass"
#define S_VELOCITY            "velocity"
#define S_VISCOSITY_RATE      "visc_rate"

#define HYDRO_EVOLVE

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace detail
{

class hydro : public thermo, public base::thermo_evolve
{

  public:

    hydro(){}
    hydro( v_map_t& v_map ) :
      thermo(), base::thermo_evolve( v_map )
    {
      d_t9_0 = v_map[nnt::s_T9_0].as<double>();
      b_hydro_equil = v_map[S_HYDRO_EQUIL].as<bool>();
      d_initial_vel = v_map[S_INITIAL_VELOCITY].as<double>();
      d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      d_accel_factor = v_map[S_ACCELERATION_FACTOR].as<double>();
      d_lum_factor = v_map[S_LUMINOSITY_FACTOR].as<double>();
      d_visc_rate = v_map[S_VISCOSITY_RATE].as<double>();
      if( v_map.count( S_MASS ) && b_hydro_equil )
      {
        std::cerr <<
          "Cannot specify both " << S_HYDRO_EQUIL << " and " << S_MASS << ".\n";
        exit( EXIT_FAILURE );
      }
      if( v_map.count( S_MASS ) )
      {
        d_mass = v_map[S_MASS].as<double>() * GSL_CONST_CGSM_SOLAR_MASS;
      }
    }

    state_type
    initializeX( nnt::Zone& zone )
    {
      state_type x;

      x.push_back( 1 );
      x.push_back( d_initial_vel );
      x.push_back( 0. );
      return x;
    }

    void
    setT9andRho( state_type& x, nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_T9, d_t9_0 );
      zone.updateProperty( nnt::s_RHO, computeRho( x, 0., zone ) );
    }

    void
    initialize( state_type& x, nnt::Zone& zone )
    {

      registerThermoFunctions( zone );

      x[2] = computeEntropy( zone );

      double d_rho_0 = zone.getProperty<double>( nnt::s_RHO );
      d_P_0 = computePressure( zone );

      if( !b_hydro_equil )
      {
        d_R_0 = pow( 3. * d_mass / ( 4. * M_PI * d_rho_0 ), 1. / 3. );
      }
      else
      {
        d_R_0 =
          pow(
            ( 3.* d_P_0 ) /
            ( 4.* M_PI * GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT *
              gsl_pow_2( d_rho_0 )
            ),
            1. / 2.
          );
        d_mass = 4. * d_rho_0 * M_PI * gsl_pow_3( d_R_0 ) / 3.;
      }
          
      d_tau_0_2 = d_rho_0 * gsl_pow_2( d_R_0 ) / d_P_0;
      d_tau_1_2 =
        gsl_pow_3( d_R_0 ) / ( GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT * d_mass );

    }

    void
    updateOtherZoneProperties(const state_type& x, nnt::Zone& zone )
    {

      zone.updateProperty( nnt::s_RADIUS, x[0] * d_R_0 );
      zone.updateProperty( S_VELOCITY, x[1] * d_R_0 );

      zone.updateProperty(
        nnt::s_YE,
        user::compute_cluster_abundance_moment( zone, "", "z", 1 )
      );

      zone.updateProperty(
        nnt::s_PRESSURE,
        computePressure( zone )
      );

    }

    double computeRho( const state_type& x, double d_t, nnt::Zone& zone )
    {
      return d_rho_0 / gsl_pow_3( x[0] );
    }

    double
    computeAcceleration(
      const state_type& x,
      const double time,
      nnt::Zone& zone
    )
    {
      double d_P = computePressure( zone );
      return
        d_accel_factor *
        ( gsl_pow_2( x[0] ) / d_tau_0_2 )
        *
        (
          ( d_P / d_P_0 ) -
          ( d_tau_0_2 / d_tau_1_2 ) * ( 1. / gsl_pow_4( x[0] ) )
        )
        -
        d_visc_rate * gsl_pow_3( x[0] ) * x[1];
    }

    double
    computeHeatingRatePerNucleon(
      const state_type& x,
      const double time,
      nnt::Zone& zone
    )
    {
      return
        d_visc_rate * gsl_pow_2( d_R_0 ) * gsl_pow_3( x[0] ) *
        gsl_pow_2( x[1] ) / GSL_CONST_NUM_AVOGADRO
        -
        d_lum_factor *
        3. * GSL_CONST_CGSM_STEFAN_BOLTZMANN_CONSTANT *
        gsl_pow_4( GSL_CONST_NUM_GIGA * zone.getProperty<double>( nnt::s_T9 ) )
        *
        gsl_pow_2( x[0] ) /
        (
          d_rho_0 * GSL_CONST_NUM_AVOGADRO * d_R_0
        );
    }


  private:
    double d_mass, d_t9_0, d_rho_0, d_P_0, d_R_0, d_accel_factor, d_lum_factor;
    double d_initial_vel;
    double d_tau_0_2, d_tau_1_2, d_visc_rate;
    bool b_hydro_equil;

};

//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options : public base::hydro_options,
                      public base::thermo_evolve_options
{

  public:
    hydro_options() : base::hydro_options(), base::thermo_evolve_options() {}

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial T (in 10^9 K)"
          )

          ( nnt::s_RHO_0, po::value<double>()->default_value( 1.e12, "1.e12" ),
            "Initial density (in g/cc)"
          )

          ( S_ACCELERATION_FACTOR,
            po::value<double>()->default_value( 1., "1." ),
            "Acceleration factor"
          )

          ( S_INITIAL_VELOCITY,
            po::value<double>()->default_value( 0., "0." ),
            "Initial scaled velocity"
          )

          ( S_VISCOSITY_RATE,
            po::value<double>()->default_value( 0., "0." ),
            "Viscosity rate"
          )

          ( S_LUMINOSITY_FACTOR,
            po::value<double>()->default_value( 0., "0." ),
            "Luminosity factor"
          )

          ( S_MASS, po::value<double>(),
            "Chunk mass (solar masses)"
          )

          ( S_HYDRO_EQUIL,
            po::value<bool>()->default_value( false, "false" ),
            "Start the sphere in hydrostatic equilibrium"
          )

        ;

        base::hydro_options::getOptions( hydro );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

      base::thermo_evolve_options::getOptions( hydro );

    }

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( S_MASS ) + " 0.01 " +
        "--" + std::string( nnt::s_RHO_0 ) + " 1.e12 " +
        base::thermo_evolve_options::getExample();
    
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
