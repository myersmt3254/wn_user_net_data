////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro_base.hpp
//! \brief A file to define base hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "boost/variant.hpp"

#ifndef NNP_HYDRO_STEPPER_HPP
#define NNP_HYDRO_STEPPER_HPP

#define S_AB6              "ab6"
#define S_DOPRI5           "dopri5"
#define S_EULER            "euler"
#define S_RK4              "rk4"
#define S_STEPPER          "stepper"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

typedef
boost::variant<
  boost::numeric::odeint::adams_bashforth<6, state_type >,
  boost::numeric::odeint::runge_kutta_dopri5< state_type >,
  boost::numeric::odeint::euler< state_type >,
  boost::numeric::odeint::runge_kutta4< state_type >
> Stepper;

//##############################################################################
// check_stepper().
//##############################################################################

void
check_stepper( const std::string& s )
{
  std::set<std::string> steppers;

  steppers.insert( S_AB6 );
  steppers.insert( S_DOPRI5 );
  steppers.insert( S_EULER );
  steppers.insert( S_RK4 );

  if( steppers.find( s ) == steppers.end() )
  {
    std::cerr << "stepper type " << s << " is not valid." << std::endl;
    std::cerr << "\nValid steppers are:\n";
    BOOST_FOREACH( std::string st, steppers )
    {
      std::cerr << "  " << st << std::endl;
    }
    std::cerr << "\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// hydro_stepper().
//##############################################################################

class hydro_stepper
{

  public:
    hydro_stepper( v_map_t& v_map )
    {
      s_stepper = v_map[S_STEPPER].as<std::string>();
    }

    Stepper
    getStepper()
    {
      Stepper stepper;
      if( s_stepper == S_AB6 )
      {
        boost::numeric::odeint::adams_bashforth<6, state_type > s;
        stepper = s;
      }
      else if( s_stepper == S_DOPRI5 )
      {
        boost::numeric::odeint::runge_kutta_dopri5< state_type > s;
        stepper = s;
      }
      else if( s_stepper == S_EULER )
      {
        boost::numeric::odeint::euler< state_type > s;
        stepper = s;
      }
      else if( s_stepper == S_RK4 )
      {
        boost::numeric::odeint::runge_kutta4< state_type > s;
        stepper = s;
      }
      return stepper;
    }

  private:
    std::string s_stepper;

};
 
//##############################################################################
// hydro_stepper_options().
//##############################################################################

class hydro_stepper_options
{

  public:
    hydro_stepper_options(){}

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( S_STEPPER,
            po::value<std::string>()->default_value( S_AB6 )->notifier(
              boost::bind( &check_stepper, _1 )
            ),
            "Set stepper type (set to \"help\" to get list)"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // NNP_HYDRO_STEPPER_HPP
