////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file libnucnet_data.hpp
//! \brief A file to define libnucnet_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef NNP_LIBNUCNET_DATA_HPP
#define NNP_LIBNUCNET_DATA_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// libnucnet_data().
//##############################################################################

class libnucnet_data : public detail::libnucnet_data
{

  public:

    libnucnet_data( v_map_t& v_map ) : detail::libnucnet_data( v_map ) {}

};
    
//##############################################################################
// libnucnet_data_options().
//##############################################################################

class libnucnet_data_options : public detail::libnucnet_data_options
{

  public:
    libnucnet_data_options() : detail::libnucnet_data_options() {}

   std::string
    getExample()
    {

      return detail::libnucnet_data_options::getExample();

    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description libnucnet_data( "\nlibnucnet data Options" );

        detail::libnucnet_data_options::getOptions( libnucnet_data );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "libnucnet_data",
            options_struct( libnucnet_data, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace wn_user

#endif // NNP_LIBNUCNET_DATA_HPP
