////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file flow_computer.hpp
//! \brief A file to define flow_computation routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef NNP_FLOW_COMPUTER_HPP
#define NNP_FLOW_COMPUTER_HPP

/**
 * @brief A namespace for user-defined flow functions.
 */
namespace wn_user
{

typedef std::map<std::string, std::pair<double, double> > flow_map_t;

typedef std::vector<flow_map_t> flow_map_vector_t;

class flow_computer : public detail::flow_computer
{

  public:

    flow_computer() {}
    flow_computer( v_map_t& v_map ) : detail::flow_computer( v_map ){}

    flow_map_t
    operator()( nnt::Zone& zone )
    {
      return computeFlowMap( zone );
    }

    flow_map_vector_t
    operator()( std::vector<nnt::Zone>& zones )
    {
      flow_map_vector_t v( zones.size() );
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        v[i] = computeFlowMap( zones[i] );
        Libnucnet__Zone__clearNetViews( zones[i].getNucnetZone() );
        Libnucnet__Zone__clearRates( zones[i].getNucnetZone() );
      }
      return v;
    }

};

//##############################################################################
// flow_computer_options().
//##############################################################################

class flow_computer_options : detail::flow_computer_options
{

  public:
    flow_computer_options() : detail::flow_computer_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description flow_computer("\nFlow computer options");

        getDetailOptions( flow_computer );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "flow_computer",
            options_struct( flow_computer )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace wn_user

#endif // NNP_FLOW_COMPUTER_HPP
