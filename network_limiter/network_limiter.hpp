////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file network_limiter.hpp
//! \brief A file to define network limiter routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef NNP_NETWORK_LIMITER_HPP
#define NNP_NETWORK_LIMITER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// network_limiter().
//##############################################################################

class network_limiter : public detail::network_limiter
{

  public:
    network_limiter(){}
    network_limiter( v_map_t& v_map ) : detail::network_limiter( v_map ) {}

};
     
//##############################################################################
// network_limiter_options().
//##############################################################################

class network_limiter_options : detail::network_limiter_options
{

  public:
    network_limiter_options() : detail::network_limiter_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description network_limiter("\nNetwork limiter options");

        detail::network_limiter_options::get( network_limiter );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "network_limiter",
            options_struct( network_limiter )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
   }

};

} // namespace wn_user 

#endif // NNP_NETWORK_LIMITER_HPP
