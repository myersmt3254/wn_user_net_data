////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
///
/// \file
///
////////////////////////////////////////////////////////////////////////////////

/**
 * @addtogroup matrix_modifier
 *
 * @{
 */

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_SPECIFIC_SPECIES_BASE_HPP
#define NNP_SPECIFIC_SPECIES_BASE_HPP

#define S_SPECIFIC_SPECIES   "specific_species"

namespace wn_user
{

namespace base
{

//##############################################################################
// specific_species().
//##############################################################################

class specific_species
{

  public:
    specific_species(){}

    void
    modifyMatrixForSpecificSpecies(
      WnMatrix * p_matrix,
      gsl_vector * p_rhs,
      nnt::Zone& zone,
      std::string s_species,
      double d_abund
    )
    {

      Libnucnet__Species * p_fixed_species =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          s_species.c_str()
        );

      if( !p_fixed_species )
      {
        std::cerr << s_species << " is not a valid species." << std::endl;
        exit( EXIT_FAILURE );
      }

      size_t i_index = Libnucnet__Species__getIndex( p_fixed_species );

      WnMatrix__removeRow( p_matrix, i_index + 1 );
  
      gsl_vector * p_row =
        gsl_vector_calloc( WnMatrix__getNumberOfColumns( p_matrix ) );

      gsl_vector_set(
        p_row,
        i_index,
        1.
      );

      WnMatrix__insertRow( p_matrix, i_index + 1, p_row );

      gsl_vector_free( p_row );

      gsl_vector_set(
        p_rhs,
        i_index,
        d_abund
        -
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          p_fixed_species
        )
      );

    }

};

}  // namespace base

}  // namespace wn_user

#endif // NNP_SPECIFIC_SPECIES_BASE_HPP

/** @} */

