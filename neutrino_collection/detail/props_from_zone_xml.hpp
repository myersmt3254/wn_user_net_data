// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define default neutrino routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>
#include <boost/math/special_functions/zeta.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"

#include "neutrino_collection/base/neutrino_collection.hpp"

#ifndef NNP_NEUTRINO_COLLECTION_DETAIL_HPP
#define NNP_NEUTRINO_COLLECTION_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// neutrino_collection().
//##############################################################################

class neutrino_collection : public base::neutrino_collection
{
  public:
    neutrino_collection() : base::neutrino_collection(){}
    neutrino_collection( v_map_t& v_map ) : base::neutrino_collection()
    { }

    double getAverageEnergyInMeV( std::string s_neutrino, nnt::Zone& zone )
    { 
      return
        3.5 * ( boost::math::zeta( 4. ) / boost::math::zeta( 3. ) ) *
        getTemperature( s_neutrino, zone );
    }

    double getAverageEnergyInErgs( std::string s_neutrino, nnt::Zone& zone )
    { 
      return
        getAverageEnergyInMeV( s_neutrino, zone ) *
          GSL_CONST_NUM_MEGA * GSL_CONST_CGSM_ELECTRON_VOLT;
    }

    double getTemperature( std::string s_neutrino, nnt::Zone& zone )
    { 
      return zone.getProperty<double>( nnt::s_NU_T, s_neutrino );
    }

    double
    computeNeutrinoFlux( std::string s_neutrino, nnt::Zone& zone )
    {
      return zone.getProperty<double>( nnt::s_NU_FLUX, s_neutrino );
    }

};

//##############################################################################
// neutrino_collection_options().
//##############################################################################

class neutrino_collection_options
{

  public:
    neutrino_collection_options() {}

    void
    getOptions( po::options_description& neutrino_collection )
    {}

};

}  // namespace detail

}  // namespace wn_user

#endif  // NNP_NEUTRINO_COLLECTION_DETAIL_HPP
