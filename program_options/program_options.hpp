//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//! \brief Code for program options.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef NNP_PROGRAM_OPTIONS_HPP
#define NNP_PROGRAM_OPTIONS_HPP

#include "my_global_types.h"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

/**
 * @brief A namespace for user-defined helper functions.
 */

namespace wn_user
{

//##############################################################################
// program_options().
//##############################################################################

class program_options
{

  public:
    program_options(){}

    static
    std::pair<std::string,std::string>
    atOptionParser( std::string const& s )
    {
      if ( '@' == s[0] )
        return std::make_pair( std::string( "response-file" ), s.substr( 1 ) );
      else
        return std::pair<std::string,std::string>();
    }

    po::variables_map 
    responseFile( 
      po::variables_map& v_map,  
      po::options_description& all 
    )
    {
      // Load the file and tokenize it
      std::ifstream ifs( v_map["response-file"].as<std::string>().c_str() );
      if( !ifs )
      {
        std::cout << "Could not open the response file\n";
        exit( EXIT_FAILURE );
      }

      // Read the whole file into a string
      std::stringstream ss;
      ss << ifs.rdbuf();

      // Split the file content
      std::string sep1("");
      std::string sep2(" \n\r");
      std::string sep3("\"");
      std::string sstr = ss.str();

      boost::escaped_list_separator<char> els( sep1, sep2, sep3 );
      boost::tokenizer< boost::escaped_list_separator<char> > tok( sstr, els );

      std::vector<std::string> args;
      std::copy( tok.begin(), tok.end(), std::back_inserter( args ) );

      // Remove empty string from end of args, if present
      if( args[args.size() - 1].empty() ) { args.pop_back(); }

      // Parse the file and store the options
      store( po::command_line_parser( args ).options( all ).run(), v_map );    

      return v_map; 
    }

    std::string
    composeOptionStringFromVector(
      const po::variables_map& vmap,
      const char * s_option
    )
    {
      std::string result( "" );
      if( vmap.count( s_option ) )
      {
        BOOST_FOREACH(
          const std::string s,
          vmap[s_option].as<std::vector<std::string> >()
        )
        {
          result += s + " ";
        }
        boost::trim( result );
      }
      return result;
    }

    std::vector<std::vector<std::string> >
    composeOptionVectorOfVectors(
      const po::variables_map& v_map,
      const char * s_option,
      std::string s_sep1,
      std::string s_sep2,
      size_t i_expected
    )
    {

      std::vector<std::vector<std::string> > result;

      if( v_map.count(s_option) == 0 )
      {
        return result;
      }

      std::stringstream ss;
      std::vector<std::string>
        sv = v_map[s_option].as<std::vector<std::string> >();
      std::copy(
        sv.begin(),
        sv.end(),
        std::ostream_iterator<std::string>( ss )
      );

      typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
      boost::char_separator<char> sep1( s_sep1.c_str() );
      std::string s_s = ss.str();
      tokenizer tokens1( s_s, sep1 );

      std::vector<std::string> v_s;
      std::copy( tokens1.begin(), tokens1.end(), std::back_inserter( v_s ) );

      BOOST_FOREACH( std::string s, v_s )
      {

        boost::char_separator<char> sep2( s_sep2.c_str() );
        tokenizer tokens2( s, sep2 );

        std::vector<std::string> v_x;
        std::copy(
          tokens2.begin(), tokens2.end(), std::back_inserter( v_x )
        );

        if( v_x.size() != i_expected )
        {
          std::cerr << "Invalid input for " << s << std::endl;
          exit( EXIT_FAILURE );
        }

        for( size_t i = 0; i < v_x.size(); i++ )
        {
          boost::trim( v_x[i] );
        }

        result.push_back( v_x );

      }

      return result;

    }

    std::vector<std::vector<std::string> >
    composeOptionVectorOfVectors(
      const po::variables_map& v_map,
      const char * s_option,
      size_t i_expected
    )
    {
      return
        composeOptionVectorOfVectors(
          v_map, s_option, "{}", ";", i_expected
        );
    }

    std::vector<std::string>
    composeOptionVectorOfStrings(
      const po::variables_map& v_map,
      const char * s_option,
      std::string s_sep
    )
    {

      std::vector<std::string> v_s;

      if( v_map.count( s_option ) == 0 )
      {
        return v_s;
      }

      typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
      boost::char_separator<char> sep( s_sep.c_str() );
      std::string s_s = v_map[s_option].as<std::string>();
      tokenizer tokens( s_s, sep );

      for(
        tokenizer::iterator it = tokens.begin();
        it != tokens.end();
        it++
      )
      {
        std::string s = *it;
        boost::algorithm::trim( s );
        v_s.push_back( s );
      }
      return v_s;

    }

    std::vector<std::string>
    getVectorOfStrings(
      const po::variables_map& v_map,
      const char * s_option
    )
    {
      std::vector<std::string> v;
      v = v_map[s_option].as<std::vector<std::string> >();
      v.erase( std::remove( v.begin(), v.end(), "" ), v.end() );
      return v;
    }

};

} // namespace wn_user

#endif // NNP_PROGRAM_OPTIONS_HPP
