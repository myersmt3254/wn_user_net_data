////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/auxiliary.h"

#include "utility/invalid_reaction_remover.hpp"
#include "utility/nucnet_copier.hpp"
#include "utility/zone_copier.hpp"
#include "outputter/base/outputter.hpp"

#ifndef NNP_OUTPUTTER_XML_BASE_HPP
#define NNP_OUTPUTTER_XML_BASE_HPP

#define S_EVOLUTION_ZONE  "evolution zone"
#define S_NEW_OUTPUT      "new_output"
#define S_XML_FILE        "output_xml"
#define S_XML_FORMAT      "xml_format"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// outputter_xml().
//##############################################################################

class outputter_xml
{

  public:
    outputter_xml( v_map_t& v_map )
    {
      bRemoved = false;
      bNewOutput = v_map[S_NEW_OUTPUT].as<bool>();
      sXmlFormat = v_map[S_XML_FORMAT].as<std::string>();

      if( v_map.count( S_XML_FILE ) )
      {
        sXmlFile = v_map[S_XML_FILE].as<std::string>();
      }

    }

    ~outputter_xml(){
      if( bNewOutput )
      {
        Libnucnet__free( pOutput );
      }
    }

    void
    set( Libnucnet * p_nucnet )
    {
      pOriginal = p_nucnet;
      if( bNewOutput )
      {
        utility::nucnet_copier nc;
        pOutput = nc( p_nucnet );
      }
      else
      {
        pOutput = p_nucnet;
      }
      Libnucnet__setZoneCompareFunction(
        pOutput,
        (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
      );
    }


    std::string
    getXmlFile() const
    { return sXmlFile; }

    std::string
    getXmlFormat() const
    { return sXmlFormat; } 

    Libnucnet *
    getOutputNucnet()
    { return pOutput; }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      if( !pOutput )
      {
        std::cerr << "Output not set." << std::endl;
        exit( EXIT_FAILURE );
      }

      if( bNewOutput )
      {
        sortOutputSpecies();
        Libnucnet__freeAllZones( pOutput );
        utility::zone_copier zc( Libnucnet__getNet( pOutput ) );
        BOOST_FOREACH( nnt::Zone zone, zones )
        {
          Libnucnet__addZone(
            pOutput,
            zc( zone.getNucnetZone() )
          );
        }
      }
    }

    void operator()( nnt::Zone& zone )
    {
      if( !pOutput )
      {
        std::cerr << "Output not set." << std::endl;
        exit( EXIT_FAILURE );
      }

      size_t i_zones = Libnucnet__getNumberOfZones( pOutput );

      utility::zone_copier zc( Libnucnet__getNet( pOutput ) );

      if( bNewOutput )
      {
        sortOutputSpecies();
        std::vector<std::string>
          s_labels{ boost::lexical_cast<std::string>( i_zones ) };
        if(
          !Libnucnet__addZone(
             pOutput,
             zc( zone.getNucnetZone(), s_labels )
          )
        )
        {
          std::cerr << "Couldn't add zone!" << std::endl;
          exit( EXIT_FAILURE );
        } 
      }
      else
      {
        i_zones = notNewOutputZones.size();
        std::vector<std::string>
          s_labels{ boost::lexical_cast<std::string>( i_zones ).c_str() };
        notNewOutputZones.push_back(
          zc( zone.getNucnetZone(), s_labels )
        );
      }

    }

    void
    prepOutput1( )
    {
      if( !bNewOutput )
      {
        Libnucnet__Zone * pCurrentZone =
          Libnucnet__getZoneByLabels( pOutput, S_EVOLUTION_ZONE, "0", "0" );
        if( pCurrentZone )
        {
          pCopy = Libnucnet__Zone__copy( pCurrentZone );
          Libnucnet__removeZone( pOutput, pCurrentZone );
        }
        utility::invalid_reaction_remover irr;
        irr( Libnucnet__getNet( pOutput ) );
        Libnucnet__freeAllZones( pOutput );
        for( size_t i = 0; i < notNewOutputZones.size(); i++ )
        {
          Libnucnet__addZone( pOutput, notNewOutputZones[i] );
        }
      }
    }

    void
    prepOutput2( )
    {
      if( !bNewOutput && pCopy )
      {
        Libnucnet__addZone( pOutput, pCopy );
      }
    }

  private:
    Libnucnet * pOriginal = NULL;
    Libnucnet * pOutput = NULL;
    Libnucnet__Zone * pCopy = NULL;
    std::vector<Libnucnet__Zone *> notNewOutputZones;
    std::string sXmlFile;
    std::string sXmlFormat;
    bool bNewOutput, bRemoved;
    
    void sortOutputSpecies( )
    {
      Libnucnet__Nuc__setSpeciesCompareFunction(
        Libnucnet__Net__getNuc( Libnucnet__getNet( pOutput ) ),
        Libnucnet__Nuc__getSpeciesCompareFunction(
          Libnucnet__Net__getNuc( Libnucnet__getNet( pOriginal ) )
        )
      );
      Libnucnet__Nuc__sortSpecies(
        Libnucnet__Net__getNuc( Libnucnet__getNet( pOutput ) )
      );
    }

};

//##############################################################################
// outputter_xml_options()
//##############################################################################

class outputter_xml_options
{

  public:
    outputter_xml_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_XML_FILE ) + " out.xml ";
    }

    void getOptions( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()

          ( S_XML_FILE, po::value<std::string>(),
            "Name of output xml file" )

          ( S_XML_FORMAT, po::value<std::string>()->default_value( "%.15e" ),
            "Format for xml mass fractions" )

          ( S_NEW_OUTPUT, po::value<bool>()->default_value( true, "true" ),
            "Use new output libnucnet" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // NNP_OUTPUTTER_XML_BASE_HPP

