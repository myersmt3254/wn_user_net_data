////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef NNP_SPECIES_TIMESCALE_UPDATER_HPP
#define NNP_SPECIES_TIMESCALE_UPDATER_HPP

#define S_TAU_SPECIES       "tau_species"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// species_timescale_updater().
//##############################################################################

class species_timescale_updater : public base::properties_updater
{

  public:
    species_timescale_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options()
    {
      if( v_map.count( S_TAU_SPECIES ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_m,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_TAU_SPECIES, ",", "", 1
          )
        )
        {
          v_tau_s.push_back( v_m[0] );
        }
      }
    }

    void
    updateSpeciesTimescale( nnt::Zone& zone )
    {
      BOOST_FOREACH( std::string s, v_tau_s )
      {
        zone.updateProperty(
          S_TAU_SPECIES,
          s,
          computeSpeciesTimescale( zone, s )
        );
      }
    }

    double
    computeSpeciesTimescale( nnt::Zone& zone, std::string s )
    {
      Libnucnet__Species * p_species =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          s.c_str()
        );
      if( !p_species )
      {
        std::cerr << s <<
          " is an invalid species for chemical potential." << std::endl;
        exit( EXIT_FAILURE );
      }
      double d_y =
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          p_species
        );
      double d_y_dt =
        Libnucnet__Zone__getSpeciesAbundanceChange(
          zone.getNucnetZone(),
          p_species
        );
      return zone.getProperty<double>( nnt::s_DTIME ) * d_y / d_y_dt;
    }

    void operator()( nnt::Zone& zone )
    {
      updateSpeciesTimescale( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        updateSpeciesTimescale( zones[i] );
      }
    }

  private:
    std::vector<std::string> v_tau_s;
    program_options my_program_options;

};
    
//##############################################################################
// species_timescale_updater_options().
//##############################################################################

class species_timescale_updater_options :
  public base::properties_updater_options
{

  public:
    species_timescale_updater_options() : base::properties_updater_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    operator()( po::options_description& properties_updater )
    {

      try
      {

        properties_updater.add_options()

        // Option for species timescale
        (
          S_TAU_SPECIES,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Comma-delimited list of species whose timescales are to be recorded"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_SPECIES_TIMESCALE_UPDATER_HPP
