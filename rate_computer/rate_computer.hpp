////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_computer.hpp
//! \brief A file to define rate computer routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef NNP_RATE_COMPUTER_HPP
#define NNP_RATE_COMPUTER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// rate_computer().
//##############################################################################

class rate_computer : public detail::rate_computer
{

  public:
    rate_computer(){}
    rate_computer( v_map_t& v_map ) : detail::rate_computer( v_map ){}

  void operator()( nnt::Zone& zone )
  {
    computeRates( zone );
  }

  void operator()( std::vector<nnt::Zone>& zones )
  {
    for( size_t i = 0; i < zones.size(); i++ )
    {
      computeRates( zones[i] );
    }
  }

};

//##############################################################################
// rate_computer_options().
//##############################################################################

class rate_computer_options : detail::rate_computer_options
{

  public:
    rate_computer_options() : detail::rate_computer_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description rate_computer("\nRate computer options");

        getDetailOptions( rate_computer );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "rate_computer",
            options_struct( rate_computer )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace wn_user

#endif // NNP_RATE_COMPUTER_HPP
